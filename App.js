import {Navigation} from 'react-native-navigation';
import {Provider} from 'react-redux';


import AuthScreen from '@screens/auth'
import SharePlaceScreen from '@screens/sharePlace';
import FindPlaceScreen from '@screens/findPlace';
import configureStore from '@redux/store/configureStore';
import PlaceDetailScreen from "@screens/placeDetail";
import SideDrawer from '@screens/sideDrawer';

const store = configureStore();
//Register Screens
Navigation.registerComponent(
    "awesome-places.AuthScreen",
    () => AuthScreen,
    store,
    Provider
);
Navigation.registerComponent(
    "awesome-places.SharePlaceScreen",
    () => SharePlaceScreen,
    store,
    Provider
)
;
Navigation.registerComponent(
    "awesome-places.FindPlaceScreen",
    () => FindPlaceScreen,
    store,
    Provider
)
;
Navigation.registerComponent(
    "awesome-places.PlaceDetailScreen",
    () => PlaceDetailScreen,
    store,
    Provider
);
Navigation.registerComponent(
    "awesome-places.SideDrawer",
    ()=>SideDrawer
    );
// Start a App

Navigation.startSingleScreenApp({
    screen: {
        screen: "awesome-places.AuthScreen",
        title: "Login"
    }
});