import React from 'react'
import {TextInput, View} from 'react-native'
import styles from "./styles";

const Index = props => (
    <TextInput
        underlineColorAndroid="transparent"
        {...props}
        style={[styles.input, props.style, !props.valid && props.touched ? styles.invalid : null]}
    />
);

export default Index;