import React from 'react';
import {Text} from 'react-native';
import styles from './styles';
const HeadingText = props => (
    <Text{...props} style={[styles.textHedaing, props.styles]}>{props.children}</Text>
);

export default HeadingText;