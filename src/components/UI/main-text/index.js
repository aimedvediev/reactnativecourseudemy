import React from 'react';
import {Text} from 'react-native';

import styles from './styles';


const MainText = props => (
    <Text style={styles.mainText}>
        {props.children}
    </Text>
);

export default MainText;