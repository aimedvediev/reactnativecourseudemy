import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  button:{
      padding:10,
      margin:5,
      borderRadius:5,
      borderWidth:1,
      borderColor:'black'
    },
    disabled:{
      backgroundColor:"#eee",
        borderColor:"#aaa"
    },
    disabledText:{
      color:"#aaa"
    }

});
