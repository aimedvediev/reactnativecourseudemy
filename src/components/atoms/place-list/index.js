import React from 'react';
import {FlatList} from 'react-native';
import styles from './styles';

import ListItem from '@atoms/list-item';

const PlaceList = props => {


    return(
    <FlatList
        style={styles.listContainer}
        data={props.places}
        renderItem={(info) => (
            <ListItem
                placeName={info.item.name}
                placeImage={info.item.image}
                onItemPressed={() => props.onItemSelected(info.item.key)}
            />
        )}
    />
    );
};

export default PlaceList;
