import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    listItem: {
        width:"100%",
        marginBottom:5,
        padding :20,
        backgroundColor:"#eee",
        flexDirection:"row",
        alignItems:"center"
    },
    placeImage:{
        marginRight:8,
        height:30,
        width: 30
    }
});
