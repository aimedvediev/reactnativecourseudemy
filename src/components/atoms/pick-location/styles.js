import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        width: "100%",
        alignItems: "center"
    },
    map: {
        width: "100%",
        height: 250,

    },
    button: {
        margin: 8
    },
});
