import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container:{
        width:"100%",
        alignItems:"center"
    },
    placeholder: {
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: "#eee",
        width: "80%",
        height: 150
    },
    button:{
        margin:8
    },
    previewImage:{
        width:'100%',
        height:'100%'
    }

});
