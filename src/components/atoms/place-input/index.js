import React, {Component} from 'react';
import {Button, TextInput, View} from "react-native";
import styles from './styles';

import DefaultInput from '@UI/default-input';


const PlaceInput = props => (
    <DefaultInput
        placeholder="Place Name"
        value={props.placeData.value}
        valid={props.placeData.valid}
        touched={props.placeData.touched}
        onChangeText={props.onChangeText}
    />
);


export default PlaceInput;