import {ADD_PLACE, DELETE_PLACE, SET_PLACES, REMOVE_PLACE} from './actionTypes';
import {uiStoptLoading, uiStartLoading} from './index'

export const addPlace = (placeName, location, image) => {
    return dispatch => {
        dispatch(uiStartLoading());
        fetch("https://us-central1-reacnativecourseudemy.cloudfunctions.net/storeImage", {
            method: "POST",
            body: JSON.stringify({
                image: image.base64
            })
        })
            .catch(err => {
                console.log(err);
                alert("Something go wrong");
                dispatch(uiStoptLoading())
            })
            .then(res => res.json())
            .then(parsedRes => {
                const placeData = {
                    name: placeName,
                    location: location,
                    image: parsedRes.imageUrl
                };
                return fetch("https://reacnativecourseudemy.firebaseio.com/places.json", {
                    method: "POST",
                    body: JSON.stringify(placeData)
                })
            })
            .catch(err => {
                console.log(err);
                alert("Something go wrong");
                dispatch(uiStoptLoading())
            })
            .then(res => res.json())
            .then(parsedRes => {
                console.log(parsedRes);
                dispatch(uiStoptLoading());
            });
    };
};

export const getPlaces = () => {
    return dispatch => {
        fetch("https://reacnativecourseudemy.firebaseio.com/places.json")
            .catch(err => {
                alert("Something wrong");
                console.log(err);
            })
            .then(res => res.json())
            .then(parsedRes => {
                const places = [];
                for (let key in parsedRes) {
                    places.push({
                        ...parsedRes[key],
                        image: {
                            uri: parsedRes[key].image
                        },
                        key: key
                    });
                }
                dispatch(setPlaces(places));
            });
    };
};

export const setPlaces = places => {
    return {
        type: SET_PLACES,
        places: places
    }
};

export const deletePlace = (key) => {
    return dispatch => {
        dispatch(removePlace(key));
        return fetch("https://reacnativecourseudemy.firebaseio.com/places/" + key + ".json", {
            method: "DELETE",
        })
            .catch(err => {
                alert("Something wrong");
                console.log(err);
            })
            .then(res => res.json())
            .then(parseRes => {
                console.log("DONE!")
            });
    };
};

export const removePlace = key => {
    return {
        type: REMOVE_PLACE,
        key: key
    };
};