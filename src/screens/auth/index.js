import React, {Component} from 'react';
import {View, ImageBackground, KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback} from 'react-native';
import {connect} from 'react-redux';
import styles from './styles';

import startMainTabs from '@screens/mainTabs';
import DefaultInput from "@UI/default-input";
import HeadingText from '@UI/heading-text';
import MainText from '@UI/main-text';
import ButtonWithBackground from '@UI/background-button';
import validate from '@utility/validations';
import {tryAuth} from '@redux/actions/';


class AuthScreen extends Component {
    state = {
        authMode: "login",
        controls: {
            email: {
                value: "",
                valid: false,
                validationRules: {
                    isEmail: true
                },
                touched: false
            },
            password: {
                value: "",
                valid: false,
                validationRules: {
                    minLength: 6
                },
                touched: false
            },
            confirmPassword: {
                value: "",
                valid: false,
                validationRules: {
                    equalTo: 'password'
                },
                touched: false
            }
        }
    };
    loginHandler = () => {
        const authData = {
            email: this.state.controls.email.value,
            password: this.state.controls.password.value
        };
        this.props.onLogin(authData);
        startMainTabs();
    };

    switchAuthModelHandler = () => {
        this.setState(prevState => {
            return {
                authMode: prevState.authMode === "login" ? "signup" : "login"
            }
        })
    };

    updateInputState = (key, value) => {
        let connectedValue = {};
        if (this.state.controls[key].validationRules.equalTo) {
            const equalControl = this.state.controls[key].validationRules.equalTo;
            const equalValue = this.state.controls[equalControl].value;
            connectedValue = {
                ...connectedValue,
                equalTo: equalValue
            };
        }
        if (key === 'password') {
            connectedValue = {
                ...connectedValue,
                equalTo: value
            };
        }
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    confirmPassword: {
                        ...prevState.controls.confirmPassword,
                        valid: key === 'password' ? validate(
                            prevState.controls.confirmPassword.value,
                            prevState.controls.confirmPassword.validationRules,
                            connectedValue) : prevState.controls.confirmPassword.valid
                    },
                    [key]: {
                        ...prevState.controls[key],
                        value: value,
                        valid: validate(value, prevState.controls[key].validationRules, connectedValue),
                        touched: true

                    }
                }
            }
        })
    };

    render() {
        let headingText = null;
        let confirmPasswordControl = null;

        if (this.state.authMode === "signup") {
            confirmPasswordControl = (
                <DefaultInput
                    style={styles.input}
                    placeholder="Confirm Password"
                    value={this.state.controls.confirmPassword.value}
                    onChangeText={(val) => this.updateInputState('confirmPassword', val)}
                    valid={this.state.controls.confirmPassword.valid}
                    touched={this.state.controls.confirmPassword.touched}
                    secureTextEntry
                />
            );
        }
        return (
            <KeyboardAvoidingView style={[styles.container,]} behavior="padding">
                <ImageBackground
                    source={{uri: "https://i.pinimg.com/originals/17/83/7b/17837b86f9487939449fdfd663cb0e54.jpg"}}
                    style={[{width: '100%', height: '100%'}, styles.container]}

                >
                    <MainText>
                        <HeadingText>Please Log in</HeadingText>
                    </MainText>
                    <ButtonWithBackground
                        color="#29aaf4"
                        onPress={this.switchAuthModelHandler}
                    >Switch to {this.state.authMode === "login" ? "Sign Up" : "Login"}</ButtonWithBackground>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <View style={styles.inputContainer}>
                            <DefaultInput
                                style={styles.input}
                                placeholder="Your E-Mail Address"
                                value={this.state.controls.email.value}
                                onChangeText={(val) => this.updateInputState('email', val)}
                                valid={this.state.controls.email.valid}
                                touched={this.state.controls.email.touched}
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="email-address"

                            />
                            <DefaultInput
                                style={styles.input}
                                placeholder="Password"
                                value={this.state.controls.password.value}
                                onChangeText={(val) => this.updateInputState('password', val)}
                                valid={this.state.controls.password.valid}
                                touched={this.state.controls.password.touched}
                                secureTextEntry
                            />
                            {confirmPasswordControl}
                        </View>
                    </TouchableWithoutFeedback>
                    <ButtonWithBackground
                        color="#29aaf4"
                        onPress={this.loginHandler}
                        disabled={
                            !this.state.controls.confirmPassword.valid && this.state.authMode === "signup" ||
                            !this.state.controls.password.valid ||
                            !this.state.controls.email.valid
                        }>Submit</ButtonWithBackground>
                </ImageBackground>
            </KeyboardAvoidingView>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogin: (authData) => dispatch(tryAuth(authData))
    }
};

export default connect(null, mapDispatchToProps)(AuthScreen);