import React, {Component} from 'react'
import {View, Button, ScrollView, Text, Image, ImageBackground, ActivityIndicator} from 'react-native'
import {connect} from 'react-redux';

import styles from './styles';

import PlaceInput from '@atoms/place-input';
import {addPlace} from '@redux/actions'
import MainText from '@UI/main-text';
import HeadingText from '@UI/heading-text';
import ButtonWithBackground from '@UI/background-button';
import PickImage from '@atoms/pick-image';
import PickLocation from '@atoms/pick-location';
import validation from '@utility/validations';


class SharePlaceScreen extends Component {
    static navigatorStyle = {
        navBarButtonColor: "red"
    };
    state = {
        controls: {
            placeName: {
                value: "",
                valid: false,
                touched: false,
                validationRules: {
                    notEmpty: true
                }
            },
            location: {
                value: null,
                valid: false
            },
            image: {
                value: null,
                valid: false
            }
        }
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    onNavigatorEvent = event => {
        if (event.type === "NavBarButtonPress") {
            if (event.id === "sideDrawerToggle") {
                this.props.navigator.toggleDrawer({
                    side: "left"
                });
            }
        }
    };

    placeAddedHandler = () => {
        this.props.onAddPlace(this.state.controls.placeName.value, this.state.controls.location.value, this.state.controls.image.value);

    };

    placeNameChangedHandler = val => {
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    placeName: {
                        ...prevState.controls.placeName,
                        value: val,
                        valid: validation(val, prevState.controls.placeName.validationRules),
                        touched: true
                    }
                }
            };
        })
    };

    locationPickedHandler = location => {
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    location: {
                        value: location,
                        valid: true
                    }

                }
            }
        });
    };


    imagePickedHandler = image => {
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    image: {
                        value: image,
                        valid: true
                    }
                }
            }
        })
    };

    render() {
        let submitButton = (
            <Button
                title="Share the Place!"
                onPress={this.placeAddedHandler}
                disabled={!this.state.controls.placeName.valid || !this.state.controls.location.valid || !this.state.controls.image.valid}
            />)
        ;

        if (this.props.isLoading) {
            submitButton = <ActivityIndicator/>;
        }
        return (
            <ScrollView>
                <View style={styles.container}>
                    <MainText>
                        <HeadingText>Please Log in</HeadingText>
                    </MainText>
                    <PickImage onImagePicked={this.imagePickedHandler}/>
                    <PickLocation onLocationPick={this.locationPickedHandler}/>
                    <PlaceInput
                        placeData={this.state.controls.placeName}
                        onChangeText={this.placeNameChangedHandler}
                    />
                    <View style={styles.button}>
                        {submitButton}
                    </View>
                </View>
            </ScrollView>

        );
    }
}

const mapStateToProps = state => {
    return {
        isLoading: state.ui.isLoading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddPlace: (placeName, location, image) => dispatch(addPlace(placeName, location, image))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(SharePlaceScreen);