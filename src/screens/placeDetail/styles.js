import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        margin: 25
    },
    placeImage: {
        width: "100%",
        height: 200
    },
    placeName: {
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 28
    },
    buttonDelete: {
        alignItems: "center"
    },
    map: {
        ...StyleSheet.absoluteFillObject
    }
});
