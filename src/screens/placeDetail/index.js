import React, {Component} from 'react';
import {View, Image, Text, Button, TouchableOpacity, Platform, Dimensions} from 'react-native';
import styles from './styles';
import {connect} from 'react-redux';
import MapView from 'react-native-maps';
import Icon from 'react-native-vector-icons/Ionicons';
import {deletePlace} from '@redux/actions';

class PlaceDetailScreen extends Component {

    placeDeletedhandler = () => {
        this.props.onDeletePlace(this.props.selectedPlace.key);
        this.props.navigator.pop();
    }

    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Image
                        source={this.props.selectedPlace.image}
                        style={styles.placeImage}
                    />
                    <Text style={styles.placeName}>{this.props.selectedPlace.value}</Text>
                </View>
                <View style={styles.placeImage}>
                    <MapView initialRegion={{
                        ...this.props.selectedPlace.location,
                        latitudeDelta: 0.0122,
                        longitudeDelta:
                            Dimensions.get("window").width /
                            Dimensions.get("window").height *
                            0.0122
                    }}
                             style={styles.map}>
                        <MapView.Marker coordinate={this.props.selectedPlace.location}/>
                    </MapView>
                </View>
                <View>
                    <TouchableOpacity onPress={this.placeDeletedhandler}>
                        <View style={styles.buttonDelete}>
                            <Icon
                                size={30}
                                name={Platform.OS === "android" ? "md-trash" : "ios-trash"}
                                color='red'
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onDeletePlace: (key) => dispatch(deletePlace(key))
    };
};

export default connect(null, mapDispatchToProps)(PlaceDetailScreen);

