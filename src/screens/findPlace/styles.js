import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    searchButton: {
        borderColor: "pink",
        borderWidth: 3,
        borderRadius: 50,
        padding: 20
    },
    searchButtonText: {
        color: "pink",
        fontWeight: "bold",
        fontSize: 26,
    },
    buttonContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    }
});
