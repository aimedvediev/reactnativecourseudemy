import React, {Component} from 'react';
import {View, Text, TouchableOpacity,Platform} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/Ionicons';

class SideDrawer extends Component {
    render() {
        return (
            <View style={styles.sideDrawer}>
                <TouchableOpacity>
                    <View style={styles.drawerItem}>
                        <Icon
                            name={Platform.OS==="android"?"md-log-out":"ios-log-out"}
                            size={20}
                            color="#aaa"
                            style={styles.drawerItemIcon}
                        />
                        <Text> Sign Out</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

export default SideDrawer;