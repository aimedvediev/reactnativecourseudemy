import {StyleSheet, Dimensions} from 'react-native';

export default StyleSheet.create({
    sideDrawer: {
        paddingTop: 52,
        width: Dimensions.get("window").width * 0.8,
        backgroundColor: "white",
        flex: 1,
    },
    drawerItem: {
        flexDirection: "row",
        alignItems:"center",
        padding:15,
        backgroundColor: "#eee"
    },
    drawerItemIcon:{
        marginRight:10
    },


});
